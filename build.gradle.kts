import java.nio.file.Path
import java.io.ByteArrayInputStream

val CI_PROJECT_PATH by lazy {
  requireNotNull(System.getenv("CI_PROJECT_PATH")) {
    "Setting the environment variable CI_PROJECT_PATH is required!"
  }
}
val CI_JOB_TOKEN by lazy {
  requireNotNull(System.getenv("CI_JOB_TOKEN")) {
    "Setting the environment variable CI_JOB_TOKEN is required!"
  }
}

val dockerImages = project.fileTree(projectDir)
  .apply {
    include("openjdk/**")
    include("python/**")
  }
  .files
  .filter { it.name == "Dockerfile" }
  .map { Image(projectDir.toPath().relativize(it.toPath())) }

data class Image(val dockerfile: Path) {
  private val dockerfilePathParts = dockerfile.parent.toString().split('/')
  val imageName = dockerfilePathParts.slice(0 until dockerfilePathParts.size - 1).joinToString("-").toLowerCase()
  val imageVersion = dockerfilePathParts.last()
  val defaultTag by lazy { "registry.gitlab.com/${CI_PROJECT_PATH.toLowerCase()}/$imageName:$imageVersion" }
}

val dockerLogin by tasks.registering(Exec::class) {
  commandLine("docker", "login", "registry.gitlab.com", "-u", "gitlab-ci-token", "--password-stdin")
  doFirst {
    standardInput = ByteArrayInputStream(CI_JOB_TOKEN.toByteArray())
  }
}

val buildTasks = mutableSetOf<TaskProvider<Exec>>()

val pushTasks = dockerImages.map {
  val pull = tasks.register("dockerPull_${it.imageName}_${it.imageVersion}", Exec::class) {
    group = "Docker"
    doFirst {
      commandLine("docker", "pull", it.defaultTag)
    }
    isIgnoreExitValue = true
    dependsOn(dockerLogin)
  }
  val build = tasks.register("dockerBuild_${it.imageName}_${it.imageVersion}", Exec::class) {
    group = "Docker"
    doFirst {
      commandLine("docker", "build", "--cache-from", it.defaultTag, "-t", it.defaultTag, it.dockerfile.parent.toString())
    }
    dependsOn(pull)
  }
  buildTasks.add(build)
  tasks.register("dockerPush_${it.imageName}_${it.imageVersion}", Exec::class) {
    group = "Docker"
    doFirst {
      commandLine("docker", "push", it.defaultTag)
    }
    dependsOn(build)
  }
}

val build by tasks.registering {
  group = "Build"
  dependsOn(* buildTasks.toTypedArray())
}
val dockerPush by tasks.registering {
  group = "Docker"
  dependsOn(* pushTasks.toTypedArray())
}
