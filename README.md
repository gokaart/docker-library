# docker library for JOSM-related projects

This project provides Docker images intended for building JOSM plugins and other JOSM-related software.

> # Important:
> The images `openjdk-x-git` and `openjdk-x-josmplugin` will soon be replaced by the corresponding `openjdk:x` images.
> For a while both images are still offered in the [container registry](https://gitlab.com/JOSM/docker-library/container_registry), but only the images `openjdk:x`, `openjdk:8-openjfx` and `python-transifex:latest` are updated and the other ones will eventually be removed.

These images are available and maintained:
* [`registry.gitlab.com/josm/docker-library/openjdk:8`](./openjdk/8/Dockerfile)
* [`registry.gitlab.com/josm/docker-library/openjdk:8-openjfx`](./openjdk/8/openjfx/Dockerfile)
* [`registry.gitlab.com/josm/docker-library/openjdk:11`](./openjdk/11/Dockerfile)
* [`registry.gitlab.com/josm/docker-library/openjdk:14`](./openjdk/14/Dockerfile)
* [`registry.gitlab.com/josm/docker-library/openjdk:15`](./openjdk/15/Dockerfile)
* [`registry.gitlab.com/josm/docker-library/openjdk:16`](./openjdk/16/Dockerfile)
* [`registry.gitlab.com/josm/docker-library/openjdk:17`](./openjdk/17/Dockerfile)
* [`registry.gitlab.com/josm/docker-library/python-transifex:latest`](./python/transifex/Dockerfile)

See the [container registry](https://gitlab.com/JOSM/docker-library/container_registry) for more detailed info about the available images.

# Changes made to the base images used.

## `openjdk:x` images
These are images based on `openjdk:x-jdk-alpine` (except for `openjdk:11`, which is based on `openjdk:11-jdk-buster`) where `x` is the JDK version.

Differences to the base image:
* a basic `~/.gradle/gradle.properties` file is created (see [the Gradle docs](https://docs.gradle.org/7.1/userguide/build_environment.html) for what the properties mean)
* `git` and `openssh` are installed
* `gettext` is installed (for building JOSM plugins with i18n)
* `ttf-dejavu` is installed (to avoid [Java problems with font metrics](https://stackoverflow.com/q/8109607))
* `github.com` and `gitlab.com` are added to `~/.ssh/known_hosts`

## `openjdk:8-openjfx`
This image is based on the `openjdk:8-jdk-stretch` image from DockerHub.

It has the same differences as the other `openjdk-x` images, plus:
* `openjfx` for Java 8 is installed

## `python-transifex:latest`
This image is based on `python:3.7-alpine`.

Differences to the base image:
* the [Transifex CLI client `tx`](https://docs.transifex.com/client/introduction) is installed ([via `pip`](https://pypi.org/project/transifex-client/))
